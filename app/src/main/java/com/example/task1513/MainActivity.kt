package com.example.task1513

import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        if (isNetworkConnected())
        {
            Toast.makeText(this,"Network is connected",Toast.LENGTH_LONG).show()
            showLoading()
            textMainActivityTextView.setText("Internet is Available")
        }
        else if(!isNetworkConnected())
        {
            Toast.makeText(this,"Network not connected",Toast.LENGTH_LONG).show()
            hideLoading()
            textMainActivityTextView.setText("Internet is not available")
        }

    }
}